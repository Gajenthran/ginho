# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.2] - 2019-07-16

### Added

- Options
- jsdoc
- README.md
- Join & quit system for started game
- Redirection to avoid overload

### Changed

- Mobile media design
- Data transaction between server and client

### Removed

- Useless dependencies (npm)

## [1.0.1] - 2019-07-14

### Added

- Component design for lobby and game
- Game animation
- Mobile media design

### Changed

- Refactoring game engine (server)

## [1.0.0] - 2019-07-12

### Added

- Add lobby, game logic and rank
- Launch project in Heroku
